@include('partials.navbar')
<div class="jumbotron header-jumbotron" style="height: 17rem;">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-content-center">
            <h1 class="display-4 text-center text-uppercase"><span class="bottom-line">Online</span> Coaching</h1>
        </div>
    </div>
</div>
<br>
<br>
<div class="container">
    <div class="row we">
        <div class="col-lg-7 d-flex flex-column justify-content-between" style="margin: 20px 0;">
            <h2 class="text-uppercase"><span class="bottom-line">Op maa</span>t gemaakt</h2>
            <p>
                Voedingsschema en trainingsschema op maat gemaakt.
                eiusmod tempor incididunt ut labore et dolore magna aliqua
                JUt enim ad minim veniam. aus nostrud exercitation ulamco
                laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat.
                aus nostrud exercitation ulamco laboris nisi ut aliquip ex ea
                commodo consequat. Duis aute
            </p>
        </div>
        <div class="col-lg-5">
            <img src="/images/we.png" alt="we">
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container-fluid" style="background-color: black">
    <div class="container">
        <div class="row we">
            <div class="col-lg-5 text-center online-coaching-insta">
                <img src="/images/instarta.jpg" alt="insta" style="margin-top: -90px; margin-bottom: -130px; width: 75%;">
            </div>
            <div class="col-lg-7 text-white">
                <br>
                <br>
                <h2 class="display-4 text-uppercase"><span class="bottom-line-white" style="padding-bottom: 10px">24/7 s</span>ervice</h2>
                <br>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmoc
                    tempor incidiount ut lahore et dolore maana allauckut enim de minin
                    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                    commodo consequot. Duis cute ure dolor in reprenendent in voluotate
                    velit esse cillum dolore eu tugiat nulla pariatur. Excepteur sint occaecal
                    cupidatamnon proldent sunun cuba au oicio deseruntmoli animila
                </p>
                <br>
                <br>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col">
            <h2 class="h1 text-center text-uppercase"><span class="bottom-line">Atleet</span> reviews</h2>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 text-center" style="margin-bottom: 50px;">
            <img src="/images/work.png" style="width: 75%; border-radius: 100%" alt="work">
            <br>
            <i class="fa-solid fa-quote-right h2 text-rta"></i>
            <p class="mx-auto" style="width: 75%; border-radius: 100%">
                Lorem ipsum dolor sit amet
                consectetur adioiscina elt sec
                do eiusmod tempor inciunt ul
                laore et dore man
                (uls losum suspendisse utrices
                gravida. Risus commodo viverre
            </p>
            <b>Idriss francis</b>
        </div>
        <div class="col-lg-4 text-center" style="margin-bottom: 50px;">
            <img src="/images/work.png" style="width: 75%; border-radius: 100%" alt="work">
            <br>
            <i class="fa-solid fa-quote-right h2 text-rta"></i>
            <p class="mx-auto" style="width: 75%; border-radius: 100%">
                Lorem ipsum dolor sit amet
                consectetur adioiscina elt sec
                do eiusmod tempor inciunt ul
                laore et dore man
                (uls losum suspendisse utrices
                gravida. Risus commodo viverre
            </p>
            <b>Idriss francis</b>
        </div>
        <div class="col-lg-4 text-center" style="margin-bottom: 50px;">
            <img src="/images/work.png" style="width: 75%; border-radius: 100%" alt="work">
            <br>
            <i class="fa-solid fa-quote-right h2 text-rta"></i>
            <p class="mx-auto" style="width: 75%; border-radius: 100%">
                Lorem ipsum dolor sit amet
                consectetur adioiscina elt sec
                do eiusmod tempor inciunt ul
                laore et dore man
                (uls losum suspendisse utrices
                gravida. Risus commodo viverre
            </p>
            <b>Idriss francis</b>
        </div>
    </div>
    <div class="row">
        <div class="col text-center">
            <br>
            <br>
            <a class="btn btn-banner-red" href="#" role="button" style="width: 240px; padding: 15px 0;">Start vandaag</a>
            <br>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
@include('partials.footer')
