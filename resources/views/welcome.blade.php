<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/css/slick/slick.css"/>
    <link rel="stylesheet" href="/css/slick/slick-theme.css"/>

    <title>Road to Athlete | TRAIN HARD, TRAIN SMART</title>

    <link rel="icon" href="/favicon.png" sizes="32x32"/>

    <style>
        * {
            font-family: 'Poppins', sans-serif;
        }

        html, body {
            background-color: #010101 !important;
        }

        .navbar-brand > img.img-fluid {
            max-height: 60px;
        }

        .navbar-nav > .nav-item.active > a.nav-link {
            font-weight: bold;
        }

        .nav-link.dropdown-toggle {
            outline: 0;
        }

        .navbar-nav > .nav-item > a.nav-link:hover {
            color: lightgray;
        }

        .navbar-light .navbar-toggler {
            border-color: white;
            outline-color: transparent;
        }

        .navbar-light .navbar-toggler-icon {
            background-image: url("data:image/svg+xml;charset=utf8,%3Csvg viewBox= '0 0 30 30' xmlns= 'http://www.w3.org/2000/svg' %3E%3Cpath stroke= 'white' stroke-width= '2' stroke-linecap= 'round' stroke-miterlimit= '10' d= 'M4 7h22M4 15h22M4 23h22' /%3E%3C/svg%3E");
        }

        .jumbotron {
            border-radius: 0;
            background-color: transparent;
        }

        .btn-white {
            color: white;
            border-radius: 0;
            border: 2px solid white;
            padding: .375rem 2rem;
            background-color: transparent;
        }

        .btn-white:hover {
            color: black;
            background-color: white;
        }

        .btn-white:focus {
            box-shadow: none;
        }

        .instagram-post {
            padding: 0 2rem;
        }

        .instagram-post > img {
            border-radius: 10px;
        }

        .online-coaching {
            font-family: 'Bebas Neue', sans-serif;
        }
    </style>
</head>
<body>
<div class="container mt-3">
    <nav class="navbar navbar-expand-lg navbar-light d-flex">
        <div class="d-flex align-items-center flex-nowrap">
            <a class="navbar-brand" href="{{ route('home') }}">
                <img class="img-fluid" src="/images/logo/rtawit.png" alt="Logo white">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse ml-4" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link text-white" href="{{ route('home') }}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">Coaching</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">Store</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white" href="#">Downloads</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Over ons
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Mo Basic</a>
                    </div>
                </li>
            </ul>
        </div>
        <form class="form-inline my-2 my-lg-0 d-none">
            <i class="fas fa-search text-white"></i>
        </form>
    </nav>
</div>
<div class="container mt-2">
    <div class="jumbotron" style="background-image: url('/images/bgg.png')">
        <h1 class="online-coaching display-4 text-uppercase text-white">Online coaching</h1>
        <p class="lead text-white">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            Duis aute irure dolor in reprehenderit eu fugiat nulla pariatur.
        </p>
        <p class="lead text-white mb-5">Probeer nu 4 weken gratis!</p>
        <p class="lead text-white">
            <a class="btn btn-white text-uppercase font-weight-bold" href="#" role="button">Meer informatie</a>
        </p>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <p class="h2 text-white text-uppercase text-center my-4">Mensen die je voor zijn geweest</p>
        </div>
    </div>
    <div class="row mb-4">
        <div class="col">
            <div class="slider instagram-slider">
                <div class="instagram-post">
                    <img class="img-fluid" src="/images/insta.jpg" alt="insta">
                </div>
                <div class="instagram-post">
                    <img class="img-fluid" src="/images/insta2.jpg" alt="insta">
                </div>
                <div class="instagram-post">
                    <img class="img-fluid" src="/images/insta4.jpg" alt="insta">
                </div>
                <div class="instagram-post">
                    <img class="img-fluid" src="/images/insta.jpg" alt="insta">
                </div>
                <div class="instagram-post">
                    <img class="img-fluid" src="/images/insta2.jpg" alt="insta">
                </div>
                <div class="instagram-post">
                    <img class="img-fluid" src="/images/insta4.jpg" alt="insta">
                </div>
                <div class="instagram-post">
                    <img class="img-fluid" src="/images/insta4.jpg" alt="insta">
                </div>
                <div class="instagram-post">
                    <img class="img-fluid" src="/images/insta4.jpg" alt="insta">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="col">
        <div class="row justify-content-around">
            <p class="text-white text-center">&copy; {{ date('Y') }} Road To Athlete</p>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" integrity="sha512-RXf+QSDCUQs5uwRKaDoXt55jygZZm2V++WUZduaU/Ui/9EGp3f/2KZVahFZBKGH0s774sd3HmrhUy+SgOFQLVQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="/js/slick/slick.min.js"></script>
<script>
    $(document).ready(function () {
        $('.instagram-slider').slick({
            dots: true,
            infinite: true,
            slidesToShow: 7,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 3000,
            swipeToSlide:true,
            // TODO: set breakpoints same as bootstrap breakpoints
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    });
</script>
</body>
</html>
