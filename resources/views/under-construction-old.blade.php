<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">

    <title>Road to Athlete | TRAIN HARD, TRAIN SMART</title>

    <link rel="stylesheet" id="fusion-dynamic-css-css" href="/css/main.css" type="text/css" media="all">

    <style type="text/css" id="css-fb-visibility">@media screen and (max-width: 768px) {
            .fusion-no-small-visibility {
                display: none !important;
            }

            body:not(.fusion-builder-ui-wireframe) .sm-text-align-center {
                text-align: center !important;
            }

            body:not(.fusion-builder-ui-wireframe) .sm-text-align-left {
                text-align: left !important;
            }

            body:not(.fusion-builder-ui-wireframe) .sm-text-align-right {
                text-align: right !important;
            }

            body:not(.fusion-builder-ui-wireframe) .sm-mx-auto {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            body:not(.fusion-builder-ui-wireframe) .sm-ml-auto {
                margin-left: auto !important;
            }

            body:not(.fusion-builder-ui-wireframe) .sm-mr-auto {
                margin-right: auto !important;
            }

            body:not(.fusion-builder-ui-wireframe) .fusion-absolute-position-small {
                position: absolute;
                top: auto;
                width: 100%;
            }
        }

        @media screen and (min-width: 769px) and (max-width: 1024px) {
            .fusion-no-medium-visibility {
                display: none !important;
            }

            body:not(.fusion-builder-ui-wireframe) .md-text-align-center {
                text-align: center !important;
            }

            body:not(.fusion-builder-ui-wireframe) .md-text-align-left {
                text-align: left !important;
            }

            body:not(.fusion-builder-ui-wireframe) .md-text-align-right {
                text-align: right !important;
            }

            body:not(.fusion-builder-ui-wireframe) .md-mx-auto {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            body:not(.fusion-builder-ui-wireframe) .md-ml-auto {
                margin-left: auto !important;
            }

            body:not(.fusion-builder-ui-wireframe) .md-mr-auto {
                margin-right: auto !important;
            }

            body:not(.fusion-builder-ui-wireframe) .fusion-absolute-position-medium {
                position: absolute;
                top: auto;
                width: 100%;
            }
        }

        @media screen and (min-width: 1025px) {
            .fusion-no-large-visibility {
                display: none !important;
            }

            body:not(.fusion-builder-ui-wireframe) .lg-text-align-center {
                text-align: center !important;
            }

            body:not(.fusion-builder-ui-wireframe) .lg-text-align-left {
                text-align: left !important;
            }

            body:not(.fusion-builder-ui-wireframe) .lg-text-align-right {
                text-align: right !important;
            }

            body:not(.fusion-builder-ui-wireframe) .lg-mx-auto {
                margin-left: auto !important;
                margin-right: auto !important;
            }

            body:not(.fusion-builder-ui-wireframe) .lg-ml-auto {
                margin-left: auto !important;
            }

            body:not(.fusion-builder-ui-wireframe) .lg-mr-auto {
                margin-right: auto !important;
            }

            body:not(.fusion-builder-ui-wireframe) .fusion-absolute-position-large {
                position: absolute;
                top: auto;
                width: 100%;
            }
        }</style>
    <style type="text/css">.recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }</style>
    <meta name="generator" content="Powered by Slider Revolution 6.2.23 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
</head>
<body class="home page-template page-template-100-width page-template-100-width-php page page-id-2 fusion-image-hovers fusion-pagination-sizing fusion-button_size-large fusion-button_type-flat fusion-button_span-no avada-image-rollover-circle-yes avada-image-rollover-yes avada-image-rollover-direction-left fusion-body ltr no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-mobile-totop fusion-disable-outline fusion-sub-menu-fade mobile-logo-pos-left layout-wide-mode avada-has-boxed-modal-shadow- layout-scroll-offset-full avada-has-zero-margin-offset-top fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text fusion-header-layout-v1 avada-responsive avada-footer-fx-none avada-menu-highlight-style-bar fusion-search-form-clean fusion-main-menu-search-overlay fusion-avatar-circle avada-dropdown-styles avada-blog-layout-large avada-blog-archive-layout-large avada-header-shadow-no avada-menu-icon-position-left avada-has-megamenu-shadow avada-has-100-footer avada-has-breadcrumb-mobile-hidden avada-has-titlebar-hide avada-header-border-color-full-transparent avada-has-pagination-width_height avada-flyout-menu-direction-fade avada-ec-views-v1 do-animate fusion-no-touch" style="--viewportWidth:1680;" data-new-gr-c-s-check-loaded="14.1024.0" data-gr-ext-installed="">
<div id="boxed-wrapper">
    <div class="fusion-sides-frame"></div>
    <div id="wrapper" class="fusion-wrapper">
        <div id="home" style="position:relative;top:-1px;"></div>
        <header class="fusion-header-wrapper">
            <div class="fusion-header-v1 fusion-logo-alignment fusion-logo-left fusion-sticky-menu- fusion-sticky-logo-1 fusion-mobile-logo-  fusion-mobile-menu-design-modern">
                <div class="fusion-header-sticky-height"></div>
                <div class="fusion-header">
                    <div class="fusion-row">
                        <nav class="fusion-main-menu" aria-label="Main Menu">
                            <div class="fusion-overlay-search">
                                <form role="search" class="searchform fusion-search-form  fusion-search-form-clean" method="get" action="https://roadtoathlete.com/">
                                    <div class="fusion-search-form-content">
                                        <div class="fusion-search-field search-field">
                                            <label><span class="screen-reader-text">Search for:</span>
                                                <input type="search" value="" name="s" class="s" placeholder="Search..." required="" aria-required="true" aria-label="Search...">
                                            </label>
                                        </div>
                                        <div class="fusion-search-button search-button">
                                            <input type="submit" class="fusion-search-submit searchsubmit" aria-label="Search" value="">
                                        </div>
                                    </div>
                                </form>
                                <div class="fusion-search-spacer"></div>
                                <a href="https://roadtoathlete.com/#" role="button" aria-label="Close Search" class="fusion-close-search"></a>
                            </div>
                        </nav>
                        <div class="fusion-mobile-menu-icons">
                            <a href="https://roadtoathlete.com/#" class="fusion-icon awb-icon-bars" aria-label="Toggle mobile menu" aria-expanded="false"></a>
                        </div>
                        <nav class="fusion-mobile-nav-holder fusion-mobile-menu-text-align-center fusion-mobile-menu-indicator-hide" aria-label="Main Menu Mobile"></nav>
                    </div>
                </div>
            </div>
            <div class="fusion-clearfix"></div>
        </header>
        <div id="sliders-container" class="fusion-slider-visibility"></div>
        <main id="main" class="clearfix width-100">
            <div class="fusion-row" style="max-width:100%;">
                <section id="content" class="full-width">
                    <div id="post-2" class="post-2 page type-page status-publish hentry">
                        <div class="post-content">
                            <div class="fusion-fullwidth fullwidth-box fusion-builder-row-1 fusion-flex-container fusion-parallax-none darkOverlay nonhundred-percent-fullwidth non-hundred-percent-height-scrolling" style="background-color: rgba(255,255,255,0);background-image: url('/images/bg.jpg');background-position: center center;background-repeat: no-repeat;border-width: 0px 0px 0px 0px;border-color:#e2e2e2;border-style:solid;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;">
                                <div class="fusion-builder-row fusion-row fusion-flex-align-items-flex-start" style="max-width:1248px;margin-left: calc(-4% / 2 );margin-right: calc(-4% / 2 );">
                                    <div class="fusion-layout-column fusion_builder_column fusion-builder-column-0 fusion_builder_column_1_6 1_6 fusion-flex-column">
                                        <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 0px 0px 0px 0px;"></div>
                                        <style type="text/css">.fusion-body .fusion-builder-column-0 {
                                                width: 16.666666666667% !important;
                                                margin-top: 0px;
                                                margin-bottom: 0px;
                                            }

                                            .fusion-builder-column-0 > .fusion-column-wrapper {
                                                padding-top: 0px !important;
                                                padding-right: 0px !important;
                                                margin-right: 11.52%;
                                                padding-bottom: 0px !important;
                                                padding-left: 0px !important;
                                                margin-left: 11.52%;
                                            }

                                            @media only screen and (max-width: 1024px) {
                                                .fusion-body .fusion-builder-column-0 {
                                                    width: 16.666666666667% !important;
                                                }

                                                .fusion-builder-column-0 > .fusion-column-wrapper {
                                                    margin-right: 11.52%;
                                                    margin-left: 11.52%;
                                                }
                                            }

                                            @media only screen and (max-width: 768px) {
                                                .fusion-body .fusion-builder-column-0 {
                                                    width: 100% !important;
                                                }

                                                .fusion-builder-column-0 > .fusion-column-wrapper {
                                                    margin-right: 1.92%;
                                                    margin-left: 1.92%;
                                                }
                                            }</style>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion-builder-column-1 fusion_builder_column_2_3 2_3 fusion-flex-column">
                                        <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 40px 0px 40px 0px;">
                                            <div style="text-align:center;">
                                                <span class=" fusion-imageframe imageframe-none imageframe-1 hover-type-none rtaDeskLogo" style="max-width:200px;margin-top:40px;margin-bottom:20px;"><img width="571" height="203" title="rtaLogoW" src="/images/logo/rtawit.png" class="img-responsive wp-image-2338"></span>
                                            </div>
                                            <div class="maintenanceContainer">
                                                <h1 class="maintenanceTitle">Rta is in onderhoud</h1>
                                                <p class="maintenanceParagraph">Als organisatie onderscheiden wij ons in
                                                    kwaliteit. Wij zullen altijd blijven streven naar het beste van het
                                                    beste voor onze atleten. Daarom zijn we momenteel achter de schermen
                                                    hard aan het werk om een gloednieuwe website te realiseren.</p>
                                            </div>
                                        </div>
                                        <style type="text/css">.fusion-body .fusion-builder-column-1 {
                                                width: 66.666666666667% !important;
                                                margin-top: 30px;
                                                margin-bottom: 30px;
                                            }

                                            .fusion-builder-column-1 > .fusion-column-wrapper {
                                                padding-top: 40px !important;
                                                padding-right: 0px !important;
                                                margin-right: 2.88%;
                                                padding-bottom: 40px !important;
                                                padding-left: 0px !important;
                                                margin-left: 2.88%;
                                            }

                                            @media only screen and (max-width: 1024px) {
                                                .fusion-body .fusion-builder-column-1 {
                                                    width: 66.666666666667% !important;
                                                    order: 0;
                                                }

                                                .fusion-builder-column-1 > .fusion-column-wrapper {
                                                    margin-right: 2.88%;
                                                    margin-left: 2.88%;
                                                }
                                            }

                                            @media only screen and (max-width: 768px) {
                                                .fusion-body .fusion-builder-column-1 {
                                                    width: 100% !important;
                                                    order: 0;
                                                }

                                                .fusion-builder-column-1 > .fusion-column-wrapper {
                                                    margin-right: 1.92%;
                                                    margin-left: 1.92%;
                                                }
                                            }</style>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion-builder-column-2 fusion_builder_column_1_6 1_6 fusion-flex-column">
                                        <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 0px 0px 0px 0px;"></div>
                                        <style type="text/css">.fusion-body .fusion-builder-column-2 {
                                                width: 16.666666666667% !important;
                                                margin-top: 0px;
                                                margin-bottom: 0px;
                                            }

                                            .fusion-builder-column-2 > .fusion-column-wrapper {
                                                padding-top: 0px !important;
                                                padding-right: 0px !important;
                                                margin-right: 11.52%;
                                                padding-bottom: 0px !important;
                                                padding-left: 0px !important;
                                                margin-left: 11.52%;
                                            }

                                            @media only screen and (max-width: 1024px) {
                                                .fusion-body .fusion-builder-column-2 {
                                                    width: 16.666666666667% !important;
                                                }

                                                .fusion-builder-column-2 > .fusion-column-wrapper {
                                                    margin-right: 11.52%;
                                                    margin-left: 11.52%;
                                                }
                                            }

                                            @media only screen and (max-width: 768px) {
                                                .fusion-body .fusion-builder-column-2 {
                                                    width: 100% !important;
                                                }

                                                .fusion-builder-column-2 > .fusion-column-wrapper {
                                                    margin-right: 1.92%;
                                                    margin-left: 1.92%;
                                                }
                                            }</style>
                                    </div>
                                </div>
                                <style type="text/css">.fusion-body .fusion-flex-container.fusion-builder-row-1 {
                                        padding-top: 0px;
                                        margin-top: 0px;
                                        padding-right: 30px;
                                        padding-bottom: 100px;
                                        margin-bottom: 0px;
                                        padding-left: 30px;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        .fusion-body .fusion-flex-container.fusion-builder-row-1 {
                                            padding-top: 50px;
                                            padding-bottom: 50px;
                                        }
                                    }</style>
                            </div>
                            <div class="fusion-fullwidth fullwidth-box fusion-builder-row-2 fusion-flex-container imgWithOp nonhundred-percent-fullwidth non-hundred-percent-height-scrolling" style="background-color: rgba(255,255,255,0);background-position: center center;background-repeat: no-repeat;border-width: 0px 0px 0px 0px;border-color:#e2e2e2;border-style:solid;">
                                <div class="fusion-builder-row fusion-row fusion-flex-align-items-flex-start" style="max-width:1248px;margin-left: calc(-4% / 2 );margin-right: calc(-4% / 2 );">
                                    <div class="fusion-layout-column fusion_builder_column fusion-builder-column-3 fusion_builder_column_1_2 1_2 fusion-flex-column">
                                        <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 50px 0px 0px 0px;">
                                            <div class="upToDateContainer">
                                                <div class="upToDateWrapper">
                                                    <p class="upToDateTitle">Blijf up to date!</p>
                                                    <p class="upToDateText">Blijf up to date door ons te volgen op
                                                        <strong>Instagram</strong>! Daar delen wij actuele
                                                        ontwikkelingen, giveaways tips &amp; adviezen m.b.t.
                                                        training/voeding en nog veel meer</p>
                                                    <div class="instaBtnWrapper">
                                                        <a target="_blank" href="https://instagram.com/roadtoathlete" class="instaBtn">Naar
                                                            Instagram</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <style type="text/css">.fusion-body .fusion-builder-column-3 {
                                                width: 50% !important;
                                                margin-top: 0px;
                                                margin-bottom: 20px;
                                            }

                                            .fusion-builder-column-3 > .fusion-column-wrapper {
                                                padding-top: 50px !important;
                                                padding-right: 0px !important;
                                                margin-right: 3.84%;
                                                padding-bottom: 0px !important;
                                                padding-left: 0px !important;
                                                margin-left: 3.84%;
                                            }

                                            @media only screen and (max-width: 1024px) {
                                                .fusion-body .fusion-builder-column-3 {
                                                    width: 50% !important;
                                                    order: 0;
                                                }

                                                .fusion-builder-column-3 > .fusion-column-wrapper {
                                                    margin-right: 3.84%;
                                                    margin-left: 3.84%;
                                                }
                                            }

                                            @media only screen and (max-width: 768px) {
                                                .fusion-body .fusion-builder-column-3 {
                                                    width: 100% !important;
                                                    order: 0;
                                                }

                                                .fusion-builder-column-3 > .fusion-column-wrapper {
                                                    margin-right: 1.92%;
                                                    margin-left: 1.92%;
                                                }
                                            }</style>
                                    </div>
                                    <div class="fusion-layout-column fusion_builder_column fusion-builder-column-4 fusion_builder_column_1_2 1_2 fusion-flex-column">
                                        <div class="fusion-column-wrapper fusion-flex-justify-content-flex-start fusion-content-layout-column" style="background-position:left top;background-repeat:no-repeat;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;padding: 0px 0px 0px 0px;">
                                            <div style="text-align:center;">
                                                <span class=" fusion-imageframe imageframe-none imageframe-2 hover-type-none"><img width="265" height="500" title="instarta" src="/images/instarta.jpg" class="img-responsive wp-image-2310"></span>
                                            </div>
                                        </div>
                                        <style type="text/css">.fusion-body .fusion-builder-column-4 {
                                                width: 50% !important;
                                                margin-top: -80px;
                                                margin-bottom: 20px;
                                            }

                                            .fusion-builder-column-4 > .fusion-column-wrapper {
                                                padding-top: 0px !important;
                                                padding-right: 0px !important;
                                                margin-right: 3.84%;
                                                padding-bottom: 0px !important;
                                                padding-left: 0px !important;
                                                margin-left: 3.84%;
                                            }

                                            @media only screen and (max-width: 1024px) {
                                                .fusion-body .fusion-builder-column-4 {
                                                    width: 50% !important;
                                                    order: 0;
                                                    margin-top: 0;
                                                }

                                                .fusion-builder-column-4 > .fusion-column-wrapper {
                                                    margin-right: 3.84%;
                                                    margin-left: 3.84%;
                                                }
                                            }

                                            @media only screen and (max-width: 768px) {
                                                .fusion-body .fusion-builder-column-4 {
                                                    width: 100% !important;
                                                    order: 0;
                                                    margin-top: 0;
                                                }

                                                .fusion-builder-column-4 > .fusion-column-wrapper {
                                                    margin-right: 1.92%;
                                                    margin-left: 1.92%;
                                                }
                                            }</style>
                                    </div>
                                </div>
                                <style type="text/css">.fusion-body .fusion-flex-container.fusion-builder-row-2 {
                                        padding-top: 0px;
                                        margin-top: 0px;
                                        padding-right: 30px;
                                        padding-bottom: 50px;
                                        margin-bottom: 0px;
                                        padding-left: 30px;
                                    }

                                    @media only screen and (max-width: 768px) {
                                        .fusion-body .fusion-flex-container.fusion-builder-row-2 {
                                            padding-top: 50px;
                                            padding-bottom: 50px;
                                        }
                                    }</style>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </main>
        <div class="fusion-footer">
            <footer id="footer" class="fusion-footer-copyright-area fusion-footer-copyright-center">
                <div class="fusion-row">
                    <div class="fusion-copyright-content">
                        <div class="fusion-copyright-notice" style="padding-bottom: 0px;">
                            <div>
                                <p style="font-size: 13px;">© 2020 - 2021 Road To Athlete - Alle Rechten voorbehouden </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>
</body>
