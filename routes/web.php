<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('under-construction');
})->name('under-construction');

Route::get('/old-construction', function () {
    return view('under-construction-old');
})->name('under-construction-old');

Route::get('/old-home', function () {
    return view('welcome');
})->name('old_home');



Route::get('/home', function () {
    return view('index');
})->name('index');

Route::get('/online-coaching', function () {
    return view('online-coaching');
})->name('online-coaching');

Route::get('/about-us', function () {
    return view('about-us');
})->name('about-us');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');
