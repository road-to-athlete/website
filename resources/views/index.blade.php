@include('partials.navbar')
<div class="jumbotron header-jumbotron" style="height: 35rem;">
    <div class="container h-100 d-flex flex-column justify-content-center">
        <h1 class="display-5">Online Coaching</h1>
        <p class="lead" style="padding: 20px 0">This is a simple hero unit, a simple jumbotron-style <br>
            component for calling extra attention to featured <br>
            content or information.
        </p>
        <a class="btn btn-banner animate__animated animate__flash animate__delay-1s" href="/" role="button" style="width: 240px; padding: 15px 0;">Start vandaag
            <i class="fa-solid fa-angle-right"></i></a>
    </div>
</div>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="h1 text-center text-uppercase"><span class="bottom-line">Wall of</span>
                <span class="text-rta">athlete</span></h2>
            <br>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <p class="text-center">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </p>
        </div>
    </div>
    <br>
    <br>
    <div class="row wall-of-athlete">
        <div class="col-lg-4" style="margin-bottom: 50px;">
            <img src="/images/wall.jpeg" alt="wall">
        </div>
        <div class="col-lg-4" style="margin-bottom: 50px;">
            <img src="/images/wall.jpeg" alt="wall">
        </div>
        <div class="col-lg-4" style="margin-bottom: 50px;">
            <img src="/images/wall.jpeg" alt="wall">
        </div>
    </div>
</div>
<br>
<br>
<div class="jumbotron" style="background-color: #3e3e3e; border-radius: 0">
    <div class="container">
        <div class="row justify-content-between">
            <h1 class="display-5 text-white">
                Ben jij <span class="text-rta">ready</span> voor veranding? <br>
                Start <span class="text-rta">nu</span> jouw <span class="font-italic">"Road To Athlete"</span>
            </h1>
            <div class="d-flex align-items-center">
                <a class="btn btn-banner" style="padding: 15px 40px;" href="#" role="button">Start vandaag
                    <i class="fa-solid fa-angle-right"></i></a>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col">
            <h2 class="h1 text-center text-uppercase"><span class="bottom-line">Onze d</span>iensten</h2>
            <br>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 text-center">
            <img src="/images/work.png" style="width: 75%; border-radius: 100%" alt="work">
            <h5>Voedingsschema</h5>
            <p class="mx-auto" style="width: 80%">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
            </p>
        </div>
        <div class="col-lg-4 text-center">
            <img src="/images/work.png" style="width: 75%; border-radius: 100%" alt="work">
            <h5>Trainingsschema</h5>
            <p class="mx-auto" style="width: 80%">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
            </p>
        </div>
        <div class="col-lg-4 text-center">
            <img src="/images/work.png" style="width: 75%; border-radius: 100%" alt="work">
            <h5>24/7 contact</h5>
            <p class="mx-auto" style="width: 80%">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.
            </p>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row we">
        <div class="col-lg-6">
            <img src="/images/we.png" alt="we">
        </div>
        <div class="col-lg-6 d-flex flex-column justify-content-between" style="margin: 20px 0;">
            <h2 class="h1 text-uppercase"><span class="bottom-line">Wie zij</span>n wij?</h2>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
            </p>
            <a class="btn btn-banner-red" href="#" role="button" style="width: 240px; padding: 15px 0;">Start vandaag
                <i class="fa-solid fa-angle-right"></i></a>
        </div>
    </div>
</div>
<br>
<br>
<br>
<br>
<br>
<br>
@include('partials.footer')
