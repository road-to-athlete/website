<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noindex, nofollow">

    <title>Road to Athlete | TRAIN HARD, TRAIN SMART</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600&display=swap" rel="stylesheet">

    <style>
        * {
            color: #303030;
            font-family: 'Poppins', sans-serif;
        }

        html, body {
            margin: 0;
            height: 100%;
            overflow: auto;
        }

        body {
            display: flex;
        }

        .logo img {
            width: 200px;
            padding: 20px;
        }

        .coming-soon {
            flex: 1;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        .construction-text {
            font-size: 2em;
            display: flex;
            flex-direction: column;
            justify-content: center;
            text-align: center;
        }

        .socials {
            display: flex;
            align-self: center;
        }

        .socials a {
            padding: 0 10px;
        }

        .socials a svg {
            width: 40px;
        }

        .under-construction-image {
            flex: 1;
            background-image: url('/images/bg.jpg');
            background-repeat: no-repeat;
            background-size: cover;
        }

        .footer-text {
            display: flex;
            justify-content: center;
        }

        @media only screen and (max-width: 1024px) {
            .construction-text {
                font-size: 1.2em;
            }
        }

        @media only screen and (max-width: 768px) {
            .under-construction-image {
                display: none;
            }
        }
    </style>
</head>
<body>
<div class="coming-soon">
    <div class="logo">
        <img src="/images/logo.png" alt="logo">
    </div>
    <div class="construction-text">
        <h1>RTA IS UNDER <br> CONSTRUCTION</h1>
        <div class="socials">
            <a href="https://www.tiktok.com/@roadtoathlete" target="_blank">
                <svg data-name="Layer 1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M256 0C114.615 0 0 114.615 0 256s114.615 256 256 256 256-114.615 256-256S397.385 0 256 0Zm129.62 232.382c-27.184 0-53.634-8.822-74-23.75l-.162 101.5a92.457 92.457 0 1 1-80.178-91.721v49.845a43.657 43.657 0 1 0 31.288 41.876V109.333h51.275a71.773 71.773 0 0 0 71.774 71.773Z" fill="#bf3a2b" class="fill-000000"></path></svg>
            </a>
            <a href="https://www.instagram.com/roadtoathlete" target="_blank">
                <svg data-name="Layer 1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M314.757 147.525H197.243a49.717 49.717 0 0 0-49.718 49.718v117.514a49.718 49.718 0 0 0 49.718 49.718h117.514a49.718 49.718 0 0 0 49.717-49.718V197.243a49.717 49.717 0 0 0-49.717-49.718ZM256 324.391A68.391 68.391 0 1 1 324.391 256 68.391 68.391 0 0 1 256 324.391Zm71.242-122.811a16.271 16.271 0 1 1 16.27-16.271 16.271 16.271 0 0 1-16.27 16.271Z" fill="#bf3a2b" class="fill-000000"></path><path d="M256 211.545A44.455 44.455 0 1 0 300.455 256 44.455 44.455 0 0 0 256 211.545Z" fill="#bf3a2b" class="fill-000000"></path><path d="M256 0C114.615 0 0 114.615 0 256s114.615 256 256 256 256-114.615 256-256S397.385 0 256 0Zm133.333 312.5a76.836 76.836 0 0 1-76.833 76.833h-113a76.837 76.837 0 0 1-76.834-76.833v-113a76.836 76.836 0 0 1 76.834-76.833h113a76.836 76.836 0 0 1 76.833 76.833Z" fill="#bf3a2b" class="fill-000000"></path></svg>
            </a>
            <a href="https://www.facebook.com/roadtoathlete" target="_blank">
                <svg data-name="Layer 1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M256 0C114.615 0 0 114.615 0 256s114.615 256 256 256 256-114.615 256-256S397.385 0 256 0Zm67.59 153.428s-26.194-2.064-36.513 1.746c-17.056 6.3-17.462 21.034-17.462 35.084v28.694h52.149l-7.62 54.888h-44.529v135.493h-58.9V273.84h-48.971v-54.888h48.974V172.4c0-49.292 37.942-67.151 60.563-69.294s52.309 4.286 52.309 4.286Z" fill="#bf3a2b" class="fill-000000"></path></svg>
            </a>
            <a href="https://wa.me/31622280839" target="_blank">
                <svg data-name="Layer 1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M263 127.88a120.44 120.44 0 0 0-99.7 188l-11.769 45.405 47.321-11.041A120.432 120.432 0 1 0 263 127.88Zm59.22 186.186c-16.845 7.9-52.014 3.778-89.065-27.93s-46.555-65.82-41.355-83.682 25.63-21.162 29.484-20.309 23.126 31.406 20.5 34.97-15.311 16.25-15.311 16.25-.534 12.98 23.284 33.364 36.56 17.851 36.56 17.851 10.574-14.5 13.689-17.639 36.28 11.181 37.718 14.857 1.335 24.371-15.509 32.268Z" fill="#bf3a2b" class="fill-000000"></path><path d="M256 0C114.615 0 0 114.615 0 256s114.615 256 256 256 256-114.615 256-256S397.385 0 256 0Zm7 393.951a144.986 144.986 0 0 1-68.86-17.282L116.7 396l19.615-75.8A145.656 145.656 0 1 1 263 393.951Z" fill="#bf3a2b" class="fill-000000"></path></svg>
            </a>
        </div>
    </div>
    <div class="footer-text">
        <p>© 2022 Road To Athlete - all rights reserved</p>
    </div>
</div>
<div class="under-construction-image">
</div>
</body>
